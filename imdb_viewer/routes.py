from os.path import exists
from flask import request, redirect, render_template
from imdb_viewer import ia, app
from imdb_viewer.help_function import save_to_archive, check_save, detail_save
import urllib.request


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST' and request.form['search']:
        initial_search = ia.search_movie(request.form['search'])
        global movie_list
        movie_list = []
        for i in initial_search:
            if i['cover url']:
                try:
                    poster_suffix = i['cover url'][i['cover url'].rindex('.'):]
                except ValueError:
                    poster_suffix = '.jpg'
                try:
                    poster_prefix = i['cover url'][:i['cover url'].rindex('@') + 1]
                except ValueError:
                    poster_prefix = i['cover url'][:i['cover url'].rindex('.')]
                poster_url = poster_prefix + poster_suffix
                static_img_path = f'static/img/{i.movieID}{poster_suffix}'
            else:
                static_img_path = 'static/img/default.jpg'
            if str(i.movieID)[0] == '0':
                movie_id = str(i.movieID)[1:]
            else:
                movie_id = str(i.movieID)
            movie_list.append({'title': i['title'], 'movie_id': movie_id,
                               'poster_url': poster_url, 'static_img_path': static_img_path}
                              )
            if not (exists(static_img_path)):
                urllib.request.urlretrieve(url=poster_url, filename='imdb_viewer/'+static_img_path)
        return redirect('/result')
    return render_template('index.html')


@app.route('/detail/<int:movie_id>', methods=['POST', 'GET'])
def detail_movie(movie_id):
    detail = ia.get_movie(movie_id)
    for i in movie_list:
        if str(i['movie_id']) == str(movie_id):
            one_movie = i
    return render_template('detail.html', one_movie=one_movie, detail=detail)


@app.route('/result', methods=['POST', 'GET'])
def result():
    if request.method == 'GET':
        return render_template('result.html', movie_list=movie_list)
    if request.method == 'POST':
        for element in movie_list:
            if element['movie_id'] == request.form['movie_id']:
                if (not exists('archive.csv')) or not (str(element['movie_id']) in check_save()):
                    data_to_save = detail_save(element['movie_id'])
                    try:
                        data_to_save['box office']['Cumulative Worldwide Gross']
                    except KeyError:
                        data_to_save['box office']['Cumulative Worldwide Gross'] = "No data"
                    for_save = {'title': element['title'], 'movie_id': str(element['movie_id']),
                                'static_img_path': element['static_img_path'],
                                'rating': str(data_to_save['rating']), 'year': str(data_to_save['year']),
                                'plot outline': str(data_to_save['plot outline']),
                                'original air date': str(data_to_save['original air date']),
                                'Budget': data_to_save['box office']['Budget'],
                                'Worldwide Gross': data_to_save['box office']['Cumulative Worldwide Gross']
                                }
                    save_to_archive(for_save)
        return render_template('result.html', movie_list=movie_list)
