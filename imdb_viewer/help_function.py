import csv
from os.path import exists
from imdb_viewer import ia


def detail_save(movie_id):
    detail = ia.get_movie(movie_id)
    return detail


def save_to_archive(data_to_save):
    fieldnames = ['title', 'movie_id', 'static_img_path', 'rating', 'year', 'plot outline',
                  'original air date', 'Budget', 'Worldwide Gross'
                  ]
    if exists('archive.csv'):
        with open('archive.csv', 'a') as file_to_save:
            writer = csv.DictWriter(file_to_save, fieldnames=fieldnames)
            writer.writerow(data_to_save)
    else:
        with open('archive.csv', 'a') as file_to_save:
            writer = csv.DictWriter(file_to_save, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerow(data_to_save)


def check_save():
    list_ids = []
    with open('archive.csv', 'r') as file_to_read:
        for ids in csv.DictReader(file_to_read):
            list_ids.append(ids['movie_id'])
    return list_ids
